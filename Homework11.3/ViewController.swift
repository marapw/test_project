//
//  ViewController.swift
//  Homework11.3
//
//  Created by Марина Поворотная on 11.04.21.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    
    //MARK: var/lets
    let gallery = [UIImage(named: "Paris1"), UIImage(named: "Paris2"), UIImage(named: "Paris3"),  UIImage(named: "Paris4"),  UIImage(named: "Paris5"),  UIImage(named: "Paris6"), UIImage(named: "Paris7")]
    var secondImageView = UIImageView()
    var currentImage = 0
    
    
    //MARK: Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.minusButton.layer.cornerRadius = 15
        self.plusButton.layer.cornerRadius = 15
        print("Workflow")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("!")
        self.secondImageView = UIImageView(frame: self.backgroundImageView.frame)
        self.backgroundImageView.image = self.gallery[self.currentImage]
        self.secondImageView.image = self.gallery[currentImage]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.secondImageView.dropShadow()
        print(backgroundImageView.frame.size)
        print(self.secondImageView.frame.size)
    }
    
    //MARK: IBActions
    @IBAction func minusButtonPressed(_ sender: UIButton) {
        self.goToPreviousImage()
    }
    @IBAction func plusButtonPressed(_ sender: UIButton) {
        self.goToNextImage()
    }
    
    //MARK: Flow functions
    func goToNextImage() {
        print("back:\(backgroundImageView.frame.origin)")
        print("top:\(secondImageView.frame.origin)")
        self.secondImageView.frame.origin.x = self.view.bounds.width
        print("back:\(backgroundImageView.frame.origin)")
        print("top:\(secondImageView.frame.origin)")
        self.plusImageIndex()
        self.secondImageView.image = gallery[currentImage]
        UIView.animate(withDuration: 1) {
            self.secondImageView.frame.origin.x -= 414
            print("back:\(self.backgroundImageView.frame.origin)")
            print("top:\(self.secondImageView.frame.origin)")
        } completion: { (_) in
            self.backgroundImageView.image = self.gallery[self.currentImage]
        }
    }
    
    func goToPreviousImage() {
        self.minusImageIndex()
        self.backgroundImageView.image = self.gallery[self.currentImage]
        print("back:\(backgroundImageView.frame.origin)")
        print("top:\(secondImageView.frame.origin)")
        UIView.animate(withDuration: 1) {
            self.secondImageView.frame.origin.x -= self.secondImageView.frame.width + self.backgroundImageView.frame.origin.x
        } completion: { (_) in
            print("back:\(self.backgroundImageView.frame.origin)")
            print("top:\(self.secondImageView.frame.origin)")
            self.secondImageView.image = self.gallery[self.currentImage]
            self.secondImageView.frame.origin = self.backgroundImageView.frame.origin
            print("back:\(self.backgroundImageView.frame.origin)")
            print("top:\(self.secondImageView.frame.origin)")
            print()
        }
        
    }
    
    func plusImageIndex() {
        if currentImage == gallery.count - 1 {
            currentImage = 0
        }else{
            currentImage += 1
        }
    }
    
    func minusImageIndex() {
        if currentImage <= 0 {
            currentImage = self.gallery.count - 1
        }else{
            currentImage -= 1
        }
    }
}

extension UIView {
    func dropShadow() {
            layer.masksToBounds = false
            layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowRadius = 20
            
            layer.shadowPath = UIBezierPath(rect: bounds).cgPath
            layer.shouldRasterize = true
        }
}
